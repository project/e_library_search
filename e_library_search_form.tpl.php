<?php
/**
 * @file
 * Default theme implementation to present the A to Z search block.
 *
 * @param $conf
 * An array containing node specific EHIS config information
 *   - client_id
 *   - search_format
 *   - search_type
 *   - description
 *
 * @see template_preprocess()
 */
 //dpm($conf);
?>
<div class="content">
      <div class="container-inline">
        <?php print $form ?>
        <p><?php print $conf['description']; ?></p>
    </div>
</div>